<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductsRepositoryInterface
{

    /**
     * Save Products
     * @param \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface $products
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface $products
    );

    /**
     * Retrieve Products
     * @param string $productsId
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($productsId);

    /**
     * Retrieve Products matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Products
     * @param \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface $products
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface $products
    );

    /**
     * Delete Products by ID
     * @param string $productsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($productsId);
}

