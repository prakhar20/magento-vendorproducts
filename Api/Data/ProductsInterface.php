<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Api\Data;

interface ProductsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const UPDATED_AT = 'updated_at';
    const PRODUCTS_ID = 'products_id';
    const VENDOR_NOTE = 'vendor_note';
    const CREATED_AT = 'created_at';
    const SKU = 'sku';
    const VENDOR_NUMBER = 'vendor_number';
    const STORE_ID = 'store_id';

    /**
     * Get products_id
     * @return string|null
     */
    public function getProductsId();

    /**
     * Set products_id
     * @param string $productsId
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     */
    public function setProductsId($productsId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     */
    public function setSku($sku);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \PrasanSoft\VendorCatalog\Api\Data\ProductsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \PrasanSoft\VendorCatalog\Api\Data\ProductsExtensionInterface $extensionAttributes
    );

    /**
     * Get vendor_number
     * @return string|null
     */
    public function getVendorNumber();

    /**
     * Set vendor_number
     * @param string $vendorNumber
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     */
    public function setVendorNumber($vendorNumber);

    /**
     * Get vendor_note
     * @return string|null
     */
    public function getVendorNote();

    /**
     * Set vendor_note
     * @param string $vendorNote
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     */
    public function setVendorNote($vendorNote);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated_at
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set updated_at
     * @param string $updatedAt
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     */
    public function setUpdatedAt($updatedAt);


    /**
     * get Store Id
     * @return string|array
     */
    public function getStoreId();

    /**
     * set store id
     * @param string|array $storeId
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface
     */
    public function setStoreId($storeId);
}

