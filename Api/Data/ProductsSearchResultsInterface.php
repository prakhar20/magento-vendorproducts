<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Api\Data;

interface ProductsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Products list.
     * @return \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface[]
     */
    public function getItems();

    /**
     * Set sku list.
     * @param \PrasanSoft\VendorCatalog\Api\Data\ProductsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

