<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Api;

interface DeleteManagementInterface
{

    /**
     * DELETE for delete api
     * @param string $param
     * @return string
     */
    public function deleteDelete($param);
}

