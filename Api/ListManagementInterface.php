<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Api;

interface ListManagementInterface
{

    /**
     * GET for list api
     * @return string
     */
    public function getList();
}

