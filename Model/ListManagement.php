<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Model;

class ListManagement implements \PrasanSoft\VendorCatalog\Api\ListManagementInterface
{
    
	protected $product;
	
	/**
	 * [__construct 
	 * @param \PrasanSoft\VendorCatalog\Model\Products $product
	 */
	public function __construct(\PrasanSoft\VendorCatalog\Model\Products $product)
    {
    	$this->product = $product;
	}
     
    public function getList()
    {
    	return $this->product->getCollection()->getData();
    }
}

