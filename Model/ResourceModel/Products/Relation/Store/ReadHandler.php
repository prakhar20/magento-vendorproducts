<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PrasanSoft\VendorCatalog\Model\ResourceModel\Products\Relation\Store;

use PrasanSoft\VendorCatalog\Model\ResourceModel\Products;
use Magento\Framework\EntityManager\Operation\ExtensionInterface;

/**
 * Class ReadHandler
 */
class ReadHandler implements ExtensionInterface
{
    /**
     * @var Products
     */
    protected $resourceProducts;

    /**
     * @param Products $resourceProducts
     */
    public function __construct(
        Products $resourceProducts
    ) {
        $this->resourceProducts = $resourceProducts;
    }

    /**
     * @param object $entity
     * @param array $arguments
     * @return object
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute($entity, $arguments = [])
    {
        if ($entity->getId()) {
            $stores = $this->resourceProducts->lookupStoreIds((int)$entity->getId());
            $entity->setData('store_id', $stores);
            $entity->setData('stores', $stores);
        }
        return $entity;
    }
}
