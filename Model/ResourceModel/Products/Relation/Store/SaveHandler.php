<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace  PrasanSoft\VendorCatalog\Model\ResourceModel\Products\Relation\Store;

use Magento\Framework\EntityManager\Operation\ExtensionInterface;
use PrasanSoft\VendorCatalog\Api\Data\ProductsInterface;;
use PrasanSoft\VendorCatalog\Model\ResourceModel\Products;
use Magento\Framework\EntityManager\MetadataPool;

/**
 * Class SaveHandler
 */
class SaveHandler implements ExtensionInterface
{
    /**
     * @var MetadataPool
     */
    protected $metadataPool;

    /**
     * @var Products
     */
    protected $resourceProducts;

    /**
     * @param MetadataPool $metadataPool
     * @param Products $resourceProducts
     */
    public function __construct(
        MetadataPool $metadataPool,
        Products $resourceProducts
    ) {
        $this->metadataPool = $metadataPool;
        $this->resourceProducts = $resourceProducts;
    }

    /**
     * @param object $entity
     * @param array $arguments
     * @return object
     * @throws \Exception
     */
    public function execute($entity, $arguments = [])
    {
        $entityMetadata = $this->metadataPool->getMetadata(ProductsInterface::class);
        $linkField = $entityMetadata->getLinkField();

        $connection = $entityMetadata->getEntityConnection();

        $oldStores = $this->resourceProducts->lookupStoreIds((int)$entity->getId());

        $newStores = (array)$entity->getStores();

        $table = $this->resourceProducts->getTable('prasansoft_vendorcatalog_products_store');

        $delete = array_diff($oldStores, $newStores);
        if ($delete) {
            $where = [
                $linkField . ' = ?' => (int)$entity->getData($linkField),
                'store_id IN (?)' => $delete,
            ];
            $connection->delete($table, $where);
        }

        $insert = array_diff($newStores, $oldStores);

        if ($insert) {
            $data = [];
            foreach ($insert as $storeId) {
                $data[] = [
                    $linkField => (int)$entity->getData($linkField),
                    'store_id' => (int)$storeId,
                ];
            }
            $connection->insertMultiple($table, $data);
        }

        return $entity;
    }
}
