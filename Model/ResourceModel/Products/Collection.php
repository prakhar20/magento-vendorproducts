<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Model\ResourceModel\Products;

use  PrasanSoft\VendorCatalog\Api\Data\ProductsInterface;

class Collection extends \PrasanSoft\VendorCatalog\Model\ResourceModel\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'products_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \PrasanSoft\VendorCatalog\Model\Products::class,
            \PrasanSoft\VendorCatalog\Model\ResourceModel\Products::class
        );
        $this->_map['fields']['store'] = 'prasansoft_vendorcatalog_products_store.store_id';
        $this->_map['fields']['store_id'] = 'prasansoft_vendorcatalog_products_store.store_id';
        $this->_map['fields']['products_id'] = 'main_table.products_id';
    }

    protected function _afterLoad()
    {
        $entityMetadata = $this->metadataPool->getMetadata(ProductsInterface::class);

        $this->performAfterLoad('prasansoft_vendorcatalog_products_store', $entityMetadata->getLinkField());

        return parent::_afterLoad();
    }

    public function addStoreFilter($store, $withAdmin = true)
    {
        $this->performAddStoreFilter($store, $withAdmin);

        return $this;
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        // Join the 2nd Table
        $this->getSelect()
            ->join(
                ['cbs' => $this->getConnection()->getTableName('prasansoft_vendorcatalog_products_store')],
                'main_table.products_id = cbs.products_id',
                ['store_id']
            );
        return $this;

    }


}

