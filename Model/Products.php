<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace PrasanSoft\VendorCatalog\Model;

use Magento\Framework\Api\DataObjectHelper;
use PrasanSoft\VendorCatalog\Api\Data\ProductsInterface;
use PrasanSoft\VendorCatalog\Api\Data\ProductsInterfaceFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;

class Products extends \Magento\Framework\Model\AbstractExtensibleModel implements ProductsInterface
{

    protected $_eventPrefix = 'prasansoft_vendorcatalog_products';
    protected $dataObjectHelper;

    protected $productsDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ProductsInterfaceFactory $productsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \PrasanSoft\VendorCatalog\Model\ResourceModel\Products $resource
     * @param \PrasanSoft\VendorCatalog\Model\ResourceModel\Products\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ProductsInterfaceFactory $productsDataFactory,
        DataObjectHelper $dataObjectHelper,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        \PrasanSoft\VendorCatalog\Model\ResourceModel\Products $resource,
        \PrasanSoft\VendorCatalog\Model\ResourceModel\Products\Collection $resourceCollection,
        array $data = []
    ) {
        $this->productsDataFactory = $productsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve products model with products data
     * @return ProductsInterface
     */
    public function getDataModel()
    {
        $productsData = $this->getData();
        
        $productsDataObject = $this->productsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $productsDataObject,
            $productsData,
            ProductsInterface::class
        );
        
        return $productsDataObject;
    }

    /**
     * [getProductsId description]
     * @return [type] [description]
     */
    public function getProductsId()
    {
        return $this->getData(self::PRODUCTS_ID);
    }
 
    /**
     * [setProductsId description]
     * @param [type] $productsId [description]
     */
    public function setProductsId($productsId)
    {
        return $this->setData(self::PRODUCTS_ID, $productsId);
    }

    /**
     * [getSku description]
     * @return [type] [description]
     */
    public function getSku()
    {
        return $this->getData(self::SKU);
    }

    /**
     * [setSku description]
     * @param [type] $sku [description]
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * [getExtensionAttributes description]
     * @return [type] [description]
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * [setExtensionAttributes description]
     * @param \PrasanSoft\VendorCatalo\Api\Data\ProductsExtensionInterface $extensionAttributes [description]
     */
    public function setExtensionAttributes(\PrasanSoft\VendorCatalog\Api\Data\ProductsExtensionInterface $extensionAttributes){
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * [getVendorNumber description]
     * @return [type] [description]
     */
    public function getVendorNumber()
    {
        return $this->getData(self::VENDOR_NUMBER);
    }

    /**
     * [setVendorNumber description]
     * @param [type] $vendorNumber [description]
     */
    public function setVendorNumber($vendorNumber)
    {
        return $this->setData(self::VENDOR_NUMBER, $vendorNumber);
    }

    /**
     * [getVendorNote description]
     * @return [type] [description]
     */
    public function getVendorNote()
    {
        return $this->getData(self::VENDOR_NOTE);
    }

    /**
     * [setVendorNote description]
     * @param [type] $vendorNote [description]
     */
    public function setVendorNote($vendorNote)
    {
        return $this->setData(self::VENDOR_NOTE, $vendorNote);
    }

    /**
     * [getCreatedAt description]
     * @return [type] [description]
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * [setCreatedAt description]
     * @param [type] $createdAt [description]
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * [getUpdatedAt description]
     * @return [type] [description]
     */
    public function getUpdatedAt(){
        
        return $this->getData(self::UPDATED_AT);

    }

    /**
     * [setUpdatedAt description]
     * @param [type] $updatedAt [description]
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }


    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    public function getStores()
    {
    return $this->hasData('stores') ? $this->getData('stores') : (array)$this->getData('store_id');
    }
}

