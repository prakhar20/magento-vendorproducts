<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace PrasanSoft\VendorCatalog\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    const PRODUCT_DISCLAIMER_FIELD = 'vendorcatalog/genaral/product_disclaimer';
    const PRODUCT_DISCLAIMER_SHOW = 'vendorcatalog/genaral/show_disclaimer';
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @param \Magento\Framework\App\Helper\Context   $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ){
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function getProductDisclaimer() {
        return $this->scopeConfig->getValue(self::PRODUCT_DISCLAIMER_FIELD,\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}